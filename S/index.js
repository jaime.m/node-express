//Solid 
//S Principio de responsabilidad simple
//O Abierto cerrado open close
//L principio de sustitución de liskov
//I principio de segregación de la interfaz
//D inversión de dependencias.
// solid ofrece calidad 
// más robusto y eficaz
// código limpio
//Escalar el código
//bajo acoplamiento
//alta cohesión

/**
 * Acoplamiento: forma y nivel de interdependencia entre modulos de software
 * si son independientes (desacoplados)
 * si tiene bajo acomplamiento es buen diseño de software
 * mejorar la mantenibilidad del software
 * reutilización del código
 * 
 */

 /**
  * Cohesión 
  * Grado en que los elementos de un módulo permanecen juntos 
  * mucha relación dentro de los componentes de un módulo
  * fácil de mantener y testear
  */

  /**single responsability principle:
   * Una clase debe tener una y solo una razón para cambiar
   */

   //incumple
   class Player {
    constructor(id,name,win,lose,date) {
        this.id=id
        this.name=name
        this.win=win
        this.lose=lose
        this.date=date
    }


 
  }
  
